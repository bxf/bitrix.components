<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
use \Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?><div class="bitfactory_copyright"><?
	?><a href="http<?=$arParams['HTTPS']=='Y' ? 's' : ''?>://bitfactory.ru" target="_blank"<?=$arParams['NOFOLLOW'] == 'Y' ? ' rel="nofollow"' : ''?>><?=$arParams['WORD']?></a>&nbsp;|&nbsp;<b><?=   Loc::getMessage("BXF_COMPANY_NAME") ?></b><?
?></div><?