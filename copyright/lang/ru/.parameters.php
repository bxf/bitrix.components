<?php
$MESS['BITFACTORY_COPYRIGHT_WORD'] = 'Ключевое слово';
$MESS['BITFACTORY_COPYRIGHT_WORD_DEFAULT'] = 'Сайты на Битрикс';
$MESS['BITFACTORY_COPYRIGHT_SYNC_WORD'] = 'Синхронизация с цетральным сервером';
$MESS['BITFACTORY_COPYRIGHT_NOFOLLOW'] = 'Закрывать ссылку rel=nofollow';
$MESS['BITFACTORY_COPYRIGHT_HTTPS'] = 'https';
