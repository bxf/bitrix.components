<?
/** BitrixVars
 * @var array $arCurrentValues
*/
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentParameters = array(
    "PARAMETERS" => Array(
        "WORD" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('BITFACTORY_COPYRIGHT_WORD'),
			"DEFAULT" => Loc::getMessage('BITFACTORY_COPYRIGHT_WORD_DEFAULT'),
		),
		"SYNC_WORD" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('BITFACTORY_COPYRIGHT_SYNC_WORD'),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
        "NOFOLLOW" => Array(
	        "PARENT" => "BASE",
	        "NAME" => Loc::getMessage('BITFACTORY_COPYRIGHT_NOFOLLOW'),
	        "TYPE" => "CHECKBOX",
	        "DEFAULT" => "Y",
        ),
		"HTTPS" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('BITFACTORY_COPYRIGHT_HTTPS'),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"CACHE_TIME" => Array(
			"DEFAULT" => 3600,
		),
    )
);