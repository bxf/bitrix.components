<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if ($this->StartResultCache(false)) {
    if ('Y' == $arParams['SYNC_WORD']) {
        @$arParams['WORD'] = file_get_contents('http'.($arParams['HTTPS'] == 'Y' ? 's' : '').'://bitfactory.ru/links.php?domain=' . urlencode($_SERVER['HTTP_HOST']));
        if (SITE_CHARSET != 'UTF-8' && !empty($arParams['WORD']))
            $arParams['WORD'] = mb_convert_encoding(
                $arParams['WORD'],
                'cp1251',
                'utf8'
            );
    }
    $this->IncludeComponentTemplate();
} else {
    $this->AbortResultCache();
}