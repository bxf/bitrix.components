<?
/** BitrixVars
 * @var array $arCurrentValues
 * @var string $componentPath
*/
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentParameters = array(
    "PARAMETERS" => Array(
        "PATH" => Array(
	        "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_PATH'),
            "DEFAULT" => "/",
        ),
        "IMG" => Array(
	        "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_IMG'),
	        "DEFAULT" => $componentPath."/templates/.default/logo.png",
            "TYPE" => "FILE",
        ),
		"IMG_SHARE" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_IMG_SHARE'),
			"DEFAULT" => "Y",
			"TYPE" => "CHECKBOX",
		),
    )
);

$arComponentParameters['PARAMETERS']['ALT'] = Array(
	"PARENT" => "BASE",
	"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_ALT'),
	"TYPE" => "LIST",
	"VALUES" => Array(
		'SITE_NAME' => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_ALT_SITE_NAME'),
		'CUSTOM' => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_ALT_CUSTOM'),
	),
	"REFRESH" => "Y",
);

if ('CUSTOM' == $arCurrentValues['ALT'])
{
	$arComponentParameters['PARAMETERS']['ALT_CUSTOM'] = Array(
		"PARENT" => "BASE",
		"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_ALT_CUSTOM_VALUE'),
	);
}

$arComponentParameters['PARAMETERS']['RESIZE_IMAGE'] = Array(
	"PARENT" => "BASE",
	"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_RESIZE_IMAGE'),
	"TYPE" => "CHECKBOX",
	"REFRESH" => "Y",
);

if ("Y" == $arCurrentValues['RESIZE_IMAGE'])
{
	$arComponentParameters['GROUPS']['RESIZE_IMAGE'] = Array(
		'NAME' => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_RESIZE_IMAGE_PARAMS'),
	);
	$arComponentParameters['PARAMETERS']['WIDTH'] = Array(
		"PARENT" => "RESIZE_IMAGE",
		"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_WIDTH'),
		"DEFAULT" => "120",
	);
	$arComponentParameters['PARAMETERS']['HEIGHT'] = Array(
		"PARENT" => "RESIZE_IMAGE",
		"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_HEIGHT'),
		"DEFAULT" => "80",
	);
	$arComponentParameters['PARAMETERS']['RESIZE_TYPE'] = Array(
		"PARENT" => "RESIZE_IMAGE",
		"NAME" => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_RESIZE_TYPE'),
		"TYPE" => "LIST",
		"VALUES" => Array(
			BX_RESIZE_IMAGE_PROPORTIONAL => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_RESIZE_TYPE_PROPORTIONAL'),
			BX_RESIZE_IMAGE_EXACT => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_RESIZE_TYPE_EXACT'),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT => Loc::getMessage('BITFACTORY_LOGO_PARAMETER_RESIZE_TYPE_PROPORTIONAL_ALT'),
		)
	);
}