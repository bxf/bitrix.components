<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponent $this
*/
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
global $APPLICATION;
$arResult['LINK'] = $_SERVER['PHP_SELF'] == $arParams['PATH'].'index.php' ? '' : ' href="' . $arParams['PATH'] . '"';

$strImgPath = $_SERVER['DOCUMENT_ROOT'] . $arParams['IMG'];
if ('Y' == $arParams['RESIZE_IMAGE'] && strlen($arParams['IMG']) && file_exists($strImgPath)
	&& (int)$arParams['WIDTH'] && (int)$arParams['HEIGHT']
	&& in_array($arParams['RESIZE_TYPE'], Array(BX_RESIZE_IMAGE_EXACT, BX_RESIZE_IMAGE_PROPORTIONAL, BX_RESIZE_IMAGE_PROPORTIONAL_ALT)))
{
	$strUploadDirName = COption::GetOptionString("main", "upload_dir", "upload");
	$strResizedImgPath = $_SERVER['DOCUMENT_ROOT'] . "/$strUploadDirName/resize_cache/bitfactory.logo/" . implode(
			'_',
			array(
				$arParams['WIDTH'],
				$arParams['HEIGHT'],
				$arParams['RESIZE_TYPE'],
				basename($strImgPath)
			)
		);
	if (!file_exists($strResizedImgPath))
	{
		CFile::ResizeImageFile(
			$strImgPath,
			$strResizedImgPath,
			Array(
				'width' => $arParams['WIDTH'],
				'height' => $arParams['HEIGHT'],
			),
			$arParams['RESIZE_TYPE']
		);
	}
	$arParams['IMG'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $strResizedImgPath);
}

if ($arParams['ALT'] == 'CUSTOM' && strlen($arParams['ALT_CUSTOM']))
{
	$arResult['ALT'] = $arParams['ALT_CUSTOM'];
}
else
{
	$arSite = CSite::GetByID(SITE_ID)->Fetch();
	$arResult['ALT'] = $arSite['NAME'];
}
if ('N' != $arParams['IMG_SHARE']) {
	$bIsHttps = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' || (bool)$_SERVER['HTTP_X_HTTPS'];
	$APPLICATION->SetPageProperty('SHARE_IMAGE', ($bIsHttps ? 'https' : 'http') . '://'.$_SERVER['HTTP_HOST'].$arParams['IMG']);
}
$this->IncludeComponentTemplate();