<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
*/
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
?><div class="col-xs-6">
    <form action="" method="post">
        <input type="hidden" name="action" value="form_feedback_add">
        <div class="row form-row">
            <div class="col-xs-2">
                <label for="form_feedback_name" class="form-label"><?=Loc::getMessage('BITFACTORY_FORM_FEEDBACK_NAME')?></label>
            </div>
            <div class="col-xs-4">
                <input id="form_feedback_name" name="PROPERTIES[NAME]" value="<?=$arResult['PROPERTIES']['NAME']?>" type="text" class="input-text">
            </div>
        </div>
        <div class="row form-row">
            <div class="col-xs-2">
                <label for="form_feedback_email" class="form-label"><?=Loc::getMessage('BITFACTORY_FORM_FEEDBACK_EMAIL')?></label>
            </div>
            <div class="col-xs-4">
                <input id="form_feedback_email" name="PROPERTIES[EMAIL]" value="<?=$arResult['PROPERTIES']['EMAIL']?>" type="email" class="input-text">
            </div>
        </div>
        <div class="row form-row">
            <div class="col-xs-2">
                <label for="form_feedback_phone" class="form-label"><?=Loc::getMessage('BITFACTORY_FORM_FEEDBACK_PHONE')?></label>
            </div>
            <div class="col-xs-4">
                <input id="form_feedback_phone" name="PROPERTIES[PHONE]" value="<?=$arResult['PROPERTIES']['PHONE']?>" type="tel" class="input-text">
            </div>
        </div>
        <div class="row form-row">
            <div class="col-xs-2">
                <label for="form_feedback_message" class="form-label"><?=Loc::getMessage('BITFACTORY_FORM_FEEDBACK_MSG')?></label>
            </div>
            <div class="col-xs-4">
                <textarea id="form_feedback_message" name="FIELDS[PREVIEW_TEXT]" class="input-textarea"><?=$arResult['FIELDS']['PREVIEW_TEXT']?></textarea>
            </div>
        </div>
        <div class="row form-row"><?
            if (isset($arResult['MESSAGE'])) {
                ShowMessage($arResult['MESSAGE']);
            }
            ?><div class="col-xs-2 col-xs-offset-2">
                <button type="submit" class="btn btn--full">
                    <span><?=Loc::getMessage('BITFACTORY_FORM_FEEDBACK_SEND')?></span>
                </button>
            </div>
        </div>
    </form>
</div><?
