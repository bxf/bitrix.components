# Компоненты 1С-Битрикс
## Установка с помощью Composer
Объявляем зависимость:

```
#!bash

composer require bitfactory/bitrix.components dev-master
```

И дополнительно прописываем параметры:
```
#!json

{
    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:bxf/bitrix.components.git"
        }
    ],
     "extra": {
         "installer-paths": {
             "local/components/{$vendor}/": [
                 "type:bitrix-component"
             ]
         }
     }
}
```