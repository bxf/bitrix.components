<?php
/**@var array $arCurrentValues */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(
	Array(
		"all" => " "
	)
);

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(
	Array(
		"SORT" => "ASC",
	),
	Array(
		"SITE_ID" => $_REQUEST["site"],
		"TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "all" ? $arCurrentValues["IBLOCK_TYPE"] : ""),
	)
);
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("CP_BMS_IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "catalog",
			"ADDITIONAL_VALUES" => "N",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("CP_BMS_IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '1',
			"MULTIPLE" => "N",
			"ADDITIONAL_VALUES" => "N",
			"REFRESH" => "Y",
		),
		"SORT_BY" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("CP_BMS_SORT_BY"),
			"DEFAULT" => "SORT",
		),
		"SORT_ORDER" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("CP_BMS_SORT_ORDER"),
			"DEFAULT" => "ASC",
		),
		"CACHE_TIME" => Array(
			"DEFAULT" => 36000000
		),
		"PROPERTIES" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("CP_BMS_PROPERTIES"),
			"MULTIPLE" => "Y",
		),
		"FILTER_NAME" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => Loc::getMessage("CP_BMS_FILTER_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
	),
	"PROPERTY_CITY" => ""
);
