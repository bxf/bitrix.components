<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $componentPath
 * @var string $componentName
 * @var string $componentTemplate
 * @var string $componentPath
 * @var CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
$arParams["SORT_ORDER"] = trim($arParams["SORT_ORDER"]);
$arResult["ELEMENTS"] = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

if($this->StartResultCache(false, Array($arrFilter)))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
    }
    else
    {
        $arSelect = Array(
            "ID",
            "NAME",
            "DETAIL_PAGE_URL",
        );
        if (is_array($arParams['PROPERTIES']) && count($arParams['PROPERTIES'])) {
            foreach($arParams['PROPERTIES'] as $strCode) {
                $arSelect[] = 'PROPERTY_'.$strCode;
            }
        }
        $dbRes = CIBlockElement::GetList(
            Array(
                $arParams["SORT_BY"] => $arParams["SORT_ORDER"],
            ),
            array_merge(
                Array(
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ACTIVE" => "Y",
                ),
                $arrFilter
            ),
            false,
            false,
            $arSelect
        );
        while($arRes = $dbRes->GetNext())
        {
            $arElement = Array(
                "NAME" => $arRes["NAME"],
                "DETAIL_PAGE_URL" => $arRes["DETAIL_PAGE_URL"],
                "PARAMS" => Array(
                    "FROM_IBLOCK" => true,
                    "DEPTH_LEVEL" => 1,
                ),
            );
            if (is_array($arParams['PROPERTIES']) && count($arParams['PROPERTIES'])) {
                foreach($arParams['PROPERTIES'] as $strCode) {
                    if (strpos($strCode, '.')!==FALSE) {
                        $arElement['PARAMS'][$strCode] = $arRes['PROPERTY_' . str_replace('.', '_', $strCode)];
                    } else {
                        $arElement['PARAMS'][$strCode] = $arRes['PROPERTY_' . $strCode . '_VALUE'];
                    }
                }
            }
            $arResult["ELEMENTS"][] = $arElement;
        }
        $this->EndResultCache();
    }
}

$arMenuLinksNew = array();
foreach($arResult["ELEMENTS"] as $arItem)
{
    $arMenuLinksNew[] = Array(
        $arItem["NAME"],
        $arItem["DETAIL_PAGE_URL"],
        Array(),
        $arItem['PARAMS'],
    );
}

return $arMenuLinksNew;