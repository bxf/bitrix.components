if (typeof jQuery !== "undefined") {
    self.BitFactory = self.BitFactory || {};
    self.BitFactory.AjaxLoader = self.BitFactory.AjaxLoader || {};
    self.BitFactory.AjaxLoader.Duration = self.BitFactory.AjaxLoader.Duration || 200;
    self.BitFactory.AjaxLoader.Start = self.BitFactory.AjaxLoader.Start || function() {
            clearTimeout(self.ajaxTimer);
            self.ajaxTimer = setTimeout(
                function(){
                    $('#ajax_loader').show();
                },
                self.BitFactory.AjaxLoader.Duration
            );
        };
    self.BitFactory.AjaxLoader.End = self.BitFactory.AjaxLoader.End || function() {
            clearTimeout(self.ajaxTimer);
            $('#ajax_loader').hide();
        };
    $(function() {
        $('<div/>', {id: 'ajax_loader'})
            .appendTo('body')
            .hide();

        $(document)
            .ajaxError(self.BitFactory.AjaxLoader.End)
            .ajaxStart(self.BitFactory.AjaxLoader.Start)
            .ajaxStop(self.BitFactory.AjaxLoader.End);
    });
    if (typeof BX !== "undefined") {
        BX.showWait = self.BitFactory.AjaxLoader.Start;
        BX.closeWait = self.BitFactory.AjaxLoader.End;
    }
}