<?
/** BitrixVars
 * @var array $arCurrentValues
 * @var string $componentPath
*/
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentParameters = array(
    "PARAMETERS" => Array(
        "DURATION" => Array(
	        "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_AJAX_LOADER_DURATION'),
            "DEFAULT" => "200",
        ),
    )
);