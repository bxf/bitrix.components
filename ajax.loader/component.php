<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$arParams['DURATION'] = (int) $arParams['DURATION'];
$APPLICATION->AddHeadString(<<<EOT
<script>
    self.BitFactory = self.BitFactory || {};
    self.BitFactory.AjaxLoader = self.BitFactory.AjaxLoader || {};
    self.BitFactory.AjaxLoader.Duration = $arParams[DURATION];
</script>
EOT
);
$this->IncludeComponentTemplate();