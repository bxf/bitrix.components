<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentDescription = array(
    "NAME" => Loc::getMessage('BITFACTORY_SUPPORT_LINK_NAME'),
    "DESCRIPTION" => "",
    "PATH" => array(
        "ID" => "Bitfactory",
        "NAME" => Loc::getMessage('BITFACTORY_PARTNER_NAME'),
    ),
);