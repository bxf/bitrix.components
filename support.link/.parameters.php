<?
/** BitrixVars
 * @var array $arCurrentValues
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentParameters = array(
    "PARAMETERS" => Array(
        "SYNC" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_SUPPORT_LINK_SYNC'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "URL" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_SUPPORT_LINK_URL'),
            "DEFAULT" => "mailto:support@bitfactory.ru",
        ),
        "ADD_FROM" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_SUPPORT_LINK_ADD_FROM'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "URL_TITLE" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage('BITFACTORY_SUPPORT_LINK_URL_TITLE'),
            "DEFAULT" => Loc::getMessage("BITFACTORY_SUPPORT_LINK_URL_TITLE_DEFAULT"),
        ),
        "CACHE_TIME" => Array(
            "DEFAULT" => 3600,
        ),
    )
);