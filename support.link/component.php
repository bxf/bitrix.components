<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if ($this->StartResultCache(false)) {
    if ('N' != $arParams['SYNC']) {
        @$arParams = array_merge(
            json_decode(file_get_contents('https://bitfactory.ru/support.php?domain=' . urlencode($_SERVER['HTTP_HOST'])), true),
            $arParams
        );
        if (SITE_CHARSET == 'windows-1251') {
            $arParams['URL_TITLE'] = utf8win1251($arParams['URL_TITLE']);
        }
    }
    if ('N' != $arParams['ADD_FROM'] && strlen($arParams['URL'])) {
        $arLinkParts = explode('?', $arParams['URL']);
        $arLinkParts[1] .= '&subject=' . urlencode(reset(explode(':', $_SERVER['HTTP_HOST'])));
        $arParams['URL'] = implode('?', $arLinkParts);
    }
    $this->IncludeComponentTemplate();
} else {
    $this->AbortResultCache();
}