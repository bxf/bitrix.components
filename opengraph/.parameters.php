<?
/** BitrixVars
 * @var array $arCurrentValues
 * @var string $componentPath
*/
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentParameters = array(
	"GROUPS" => Array(
		"TYPE" => Array(
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_TYPE'),
		),
		"SITE_NAME" => Array(
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_SITE_NAME'),
		),
		"URL" => Array(
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_URL'),
		),
		"IMAGE" => Array(
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_IMAGE'),
		),
		"TITLE" => Array(
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_TITLE'),
		),
		"DESCRIPTION" => Array(
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_DESCRIPTION'),
		),
	),
	"PARAMETERS" => Array(),
);

$arComponentParameters['PARAMETERS']['TYPE'] = Array(
	"PARENT" => "TYPE",
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_TYPE'),
	"DEFAULT" => "WEBSITE",
	"TYPE" => "LIST",
	"REFRESH" => "Y",
	"VALUES" => Array(
		"WEBSITE" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_TYPE_WEBSITE'),
		"PAGE_PROPERTY" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
		"CUSTOM" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
		"NO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_NO'),
	)
);

switch($arCurrentValues['TYPE'])
{
	case 'PAGE_PROPERTY':
		$arComponentParameters['PARAMETERS']['TYPE_PAGE_PROPERTY'] = Array(
			"PARENT" => "TYPE",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
			"DEFAULT" => "TYPE",
		);
		break;
	case 'CUSTOM':
		$arComponentParameters['PARAMETERS']['TYPE_CUSTOM'] = Array(
			"PARENT" => "TYPE",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
			"DEFAULT" => "",
		);
		break;
}

$arComponentParameters['PARAMETERS']['SITE_NAME'] = Array(
	"PARENT" => "SITE_NAME",
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_SITE_NAME'),
	"DEFAULT" => "REQUEST",
	"TYPE" => "LIST",
	"REFRESH" => "Y",
	"VALUES" => Array(
		"REQUEST" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_SITE_NAME_REQUEST'),
		"PAGE_PROPERTY" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
		"CUSTOM" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
		"NO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_NO'),
	)
);

switch($arCurrentValues['SITE_NAME'])
{
	case 'PAGE_PROPERTY':
		$arComponentParameters['PARAMETERS']['SITE_NAME_PAGE_PROPERTY'] = Array(
			"PARENT" => "SITE_NAME",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
			"DEFAULT" => "SITE_NAME",
		);
		break;
	case 'CUSTOM':
		$arComponentParameters['PARAMETERS']['SITE_NAME_CUSTOM'] = Array(
			"PARENT" => "SITE_NAME",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
			"DEFAULT" => "",
		);
		break;
}

$arComponentParameters['PARAMETERS']['URL'] = Array(
	"PARENT" => "URL",
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_URL'),
	"DEFAULT" => "AUTO",
	"TYPE" => "LIST",
	"REFRESH" => "Y",
	"VALUES" => Array(
		"AUTO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_AUTO'),
		"PAGE_PROPERTY" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
		"CUSTOM" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
		"NO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_NO'),
	)
);

switch($arCurrentValues['URL'])
{
	case 'PAGE_PROPERTY':
		$arComponentParameters['PARAMETERS']['URL_PAGE_PROPERTY'] = Array(
			"PARENT" => "URL",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
			"DEFAULT" => "URL",
		);
		break;
	case 'CUSTOM':
		$arComponentParameters['PARAMETERS']['URL_CUSTOM'] = Array(
			"PARENT" => "URL",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
			"DEFAULT" => "",
		);
		break;
}

$arComponentParameters['PARAMETERS']['IMAGE'] = Array(
	"PARENT" => "IMAGE",
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_IMAGE'),
	"DEFAULT" => "PAGE_PROPERTY",
	"TYPE" => "LIST",
	"REFRESH" => "Y",
	"VALUES" => Array(
		"PAGE_PROPERTY" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
		"CUSTOM" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
		"NO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_NO'),
	)
);

switch($arCurrentValues['IMAGE'])
{
	case 'PAGE_PROPERTY':
		$arComponentParameters['PARAMETERS']['IMAGE_PAGE_PROPERTY'] = Array(
			"PARENT" => "IMAGE",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
			"DEFAULT" => "SHARE_IMAGE",
		);
		break;
	case 'CUSTOM':
		$arComponentParameters['PARAMETERS']['IMAGE_CUSTOM'] = Array(
			"PARENT" => "IMAGE",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
			"DEFAULT" => "",
		);
		break;
}

$arComponentParameters['PARAMETERS']['TITLE'] = Array(
	"PARENT" => "TITLE",
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_TITLE'),
	"DEFAULT" => "AUTO",
	"TYPE" => "LIST",
	"REFRESH" => "Y",
	"VALUES" => Array(
		"AUTO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_AUTO'),
		"PAGE_PROPERTY" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
		"CUSTOM" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
		"NO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_NO'),
	)
);

switch($arCurrentValues['TITLE'])
{
	case 'PAGE_PROPERTY':
		$arComponentParameters['PARAMETERS']['TITLE_PAGE_PROPERTY'] = Array(
			"PARENT" => "TITLE",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
			"DEFAULT" => "title",
		);
		break;
	case 'CUSTOM':
		$arComponentParameters['PARAMETERS']['TITLE_CUSTOM'] = Array(
			"PARENT" => "TITLE",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
			"DEFAULT" => "",
		);
		break;
}

$arComponentParameters['PARAMETERS']['DESCRIPTION'] = Array(
	"PARENT" => "DESCRIPTION",
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_DESCRIPTION'),
	"DEFAULT" => "AUTO",
	"TYPE" => "LIST",
	"REFRESH" => "Y",
	"VALUES" => Array(
		"AUTO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_AUTO'),
		"PAGE_PROPERTY" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
		"CUSTOM" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
		"NO" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_NO'),
	)
);

switch($arCurrentValues['DESCRIPTION'])
{
	case 'PAGE_PROPERTY':
		$arComponentParameters['PARAMETERS']['DESCRIPTION_PAGE_PROPERTY'] = Array(
			"PARENT" => "DESCRIPTION",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_PAGE_PROPERTY'),
			"DEFAULT" => "description",
		);
		break;
	case 'CUSTOM':
		$arComponentParameters['PARAMETERS']['DESCRIPTION_CUSTOM'] = Array(
			"PARENT" => "DESCRIPTION",
			"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPERGRAPTH_PARAMS_CUSTOM'),
			"DEFAULT" => "",
		);
		break;
}