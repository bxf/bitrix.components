<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
$bIsHttps = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' || (bool)$_SERVER['HTTP_X_HTTPS'];
//----------------------------------------------------------------------------------------------------------------------
switch($arParams['TYPE']) {
	case 'WEBSITE':
		$arResult['TYPE'] = 'website';
		break;
	case 'PAGE_PROPERTY':
		if (strlen($arParams['STYPE_PAGE_PROPERTY'])) {
			?><meta property="og:type" content="<?$APPLICATION->ShowProperty($arParams['TYPE_PAGE_PROPERTY'])?>"><?
		}
		break;
	case 'CUSTOM':
		if (strlen($arParams['TYPE_CUSTOM'])) {
			$arResult['TYPE'] = $arParams['TYPE_CUSTOM'];
		}
		break;
}
if (strlen($arResult['TYPE'])) {
	?><meta property="og:type" content="<?=$arResult['TYPE']?>"><?
}
//----------------------------------------------------------------------------------------------------------------------
switch($arParams['SITE_NAME']) {
	case 'REQUEST':
		$arSite = CSite::GetByID(SITE_ID)->Fetch();
		$arResult['SITE_NAME'] = $arSite['NAME'];
		break;
	case 'PAGE_PROPERTY':
		if (strlen($arParams['SITE_NAME_PAGE_PROPERTY'])) {
			?><meta property="og:site_name" content="<?$APPLICATION->ShowProperty($arParams['SITE_NAME_PAGE_PROPERTY'])?>"><?
		}
		break;
	case 'CUSTOM':
		if (strlen($arParams['SITE_NAME_CUSTOM'])) {
			$arResult['SITE_NAME'] = $arParams['SITE_NAME_CUSTOM'];
		}
		break;
}
if (strlen($arResult['SITE_NAME'])) {
	?><meta property="og:site_name" content="<?=$arResult['SITE_NAME']?>"><?
}
//----------------------------------------------------------------------------------------------------------------------
switch($arParams['URL']) {
	case 'AUTO':
		$arResult['URL'] = ($bIsHttps ? 'https' : 'http') . '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		break;
	case 'PAGE_PROPERTY':
		if (strlen($arParams['URL_PAGE_PROPERTY'])) {
			?><meta property="og:url" content="<?$APPLICATION->ShowProperty($arParams['URL_PAGE_PROPERTY'])?>"><?
		}
		break;
	case 'CUSTOM':
		if (strlen($arParams['URL_CUSTOM'])) {
			$arResult['URL'] = $arParams['URL_CUSTOM'];
		}
		break;
}
if (strlen($arResult['URL'])) {
	?><meta property="og:url" content="<?=$arResult['URL']?>"><?
}
//----------------------------------------------------------------------------------------------------------------------
switch($arParams['IMAGE'])
{
	case 'PAGE_PROPERTY':
		if (strlen($arParams['IMAGE_PAGE_PROPERTY'])) {
			?><meta property="og:image" content="<?$APPLICATION->ShowProperty($arParams['IMAGE_PAGE_PROPERTY'])?>"><?
		}
		break;
	case 'CUSTOM':
		if (strlen($arParams['IMAGE_CUSTOM'])) {
			$arResult['IMAGE'] = $arParams['IMAGE_CUSTOM'];
		}
		break;
}
if (strlen($arResult['IMAGE'])) {
	?><meta property="og:image" content="<?=$arResult['IMAGE']?>"><?
}
//----------------------------------------------------------------------------------------------------------------------
switch($arParams['TITLE']) {
	case 'AUTO':
		?><meta property="og:title" content="<?$APPLICATION->ShowTitle(true)?>"><?
		break;
	case 'PAGE_PROPERTY':
		if (strlen($arParams['TITLE_PAGE_PROPERTY'])) {
			?><meta property="og:title" content="<?$APPLICATION->ShowProperty($arParams['TITLE_PAGE_PROPERTY'])?>"><?
		}
		break;
	case 'CUSTOM':
		if (strlen($arParams['TITLE_CUSTOM'])) {
			$arResult['TITLE'] = $arParams['TITLE_CUSTOM'];
		}
		break;
}
if (strlen($arResult['TITLE'])) {
	?><meta property="og:title" content="<?=$arResult['TITLE']?>"><?
}
//----------------------------------------------------------------------------------------------------------------------
switch($arParams['DESCRIPTION']) {
	case 'AUTO':
		?><meta property="og:description" content="<?$APPLICATION->ShowProperty('description')?>"><?
		break;
	case 'PAGE_PROPERTY':
		if (strlen($arParams['DESCRIPTION_PAGE_PROPERTY'])) {
			?><meta property="og:description" content="<?$APPLICATION->ShowProperty($arParams['DESCRIPTION_PAGE_PROPERTY'])?>"><?
		}
		break;
	case 'CUSTOM':
		if (strlen($arParams['DESCRIPTION_CUSTOM'])) {
			$arResult['DESCRIPTION'] = $arParams['DESCRIPTION_CUSTOM'];
		}
		break;
}
if (strlen($arResult['DESCRIPTION'])) {
	?><meta property="og:description" content="<?=$arResult['DESCRIPTION']?>"><?
}
//----------------------------------------------------------------------------------------------------------------------
