<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentDescription = array(
	"NAME" => Loc::getMessage('BITFACTORY_HEADER_OPENGRAPH_NAME'),
	"DESCRIPTION" => "",
	"PATH" => array(
		"ID" => "Bitfactory",
		"NAME" => Loc::getMessage('BITFACTORY_PARTNER_NAME'),
	),
);