<?
/** BitrixVars
 * @var array $arCurrentValues
 * @var string $componentPath
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
use \Bitrix\Main\Localization\Loc;
if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(
	Array(
		"all" => " "
	)
);

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(
	Array(
		"SORT" => "ASC",
	),
	Array(
		"SITE_ID" => $_REQUEST["site"],
		"TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "all" ? $arCurrentValues["IBLOCK_TYPE"] : ""),
	)
);
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$dbRes = CEventType::GEtList(
    Array(
        'LID' => LANG,
    ),
    Array(
        'TYPE_ID' => 'ASC',
    )
);
$arMessageTypes = Array();
while ($arRes = $dbRes->Fetch()) {
    $arMessageTypes[$arRes['EVENT_NAME']] = $arRes['NAME'];
}

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('BITFACTORY_IBLOCK_TYPE'),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "forms",
			"ADDITIONAL_VALUES" => "N",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('BITFACTORY_IBLOCK'),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => constant('IBLOCK_FEEDBACK'),
			"MULTIPLE" => "N",
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
	),
);
