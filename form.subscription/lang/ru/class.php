<?php
$MESS['BITFACTORY_FORM_SUBSCRIPTION_SUCCESS'] = 'Ваш адрес успешно добавлен';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_FROM_01'] = 'Обязательное поле';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_TO_01'] = 'Поле';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_FROM_02'] = 'Обязательное свойство';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_TO_02'] = 'Поле';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_FROM_03'] = 'Не введено название';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_TO_03'] = 'Введите E-mail';
$MESS['BITFACTORY_FORM_SUBSCRIPTION_ERROR_EMAIL'] = 'Проверьте правильность ввода email';
