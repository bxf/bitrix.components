<?php
/**@global CMain $APPLICATION */
/**@global CUser $USER */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if (!CModule::IncludeModule("iblock"))
    return false;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class BitFactoryFormSubscription extends CBitrixComponent {
    public function onPrepareComponentParams($arParams) {
        $arParams['IBLOCK_TYPE'] = trim($arParams['IBLOCK_TYPE']);
        $arParams['IBLOCK_ID'] = (int)$arParams['IBLOCK_ID'];
        return $arParams;
    }

    public function executeComponent() {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        $arRequest = array_merge($_GET, $_POST);

        if ($arRequest['action'] == 'subscribe') {
            foreach (Array('FIELDS', 'PROPERTIES') as $strType) {
                foreach ($arRequest[$strType] as $strCode => $strValue) {
                    $arResult[$strType][$strCode] = trim($strValue);
                }
            }
            $obIBlockElement = new CIBlockElement();
            $arFieldsDefaults = Array(
                'NAME' => '',
            );
            $arSaveData = Array(
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'PROPERTY_VALUES' => $arResult['PROPERTIES'],
            );
            foreach ($arFieldsDefaults as $strCode => $strValue) {
                if (!strlen($arResult['FIELDS'][$strCode])) {
                    $arSaveData[$strCode] = $strValue;
                } else {
                    $arSaveData[$strCode] = $arResult['FIELDS'][$strCode];
                }
            }
            if (!check_email($arResult['FIELDS']['NAME'])){
                $arResult['MESSAGE'] = Array(
                    'TYPE' => 'ERROR',
                    'MESSAGE' => Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_EMAIL'),
                );
            } else  if ($intNewID = $obIBlockElement->Add($arSaveData)) {
                $arResult['MESSAGE'] = Array(
                    'TYPE' => 'OK',
                    'MESSAGE' => Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_SUCCESS'),
                );
                unset($arResult['FIELDS']);
                unset($arResult['PROPERTIES']);
            } else {
                $arResult['MESSAGE'] = Array(
                    'TYPE' => 'ERROR',
                    'MESSAGE' => str_replace(
                        Array(
                            Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_FROM_01'),
                            Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_FROM_02'),
                            Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_FROM_03'),
                        ),
                        Array(
                            Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_TO_01'),
                            Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_TO_02'),
                            Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_ERROR_TO_03'),
                        ),
                        $obIBlockElement->LAST_ERROR
                    ),
                );
            }
        }

        $this->includeComponentTemplate();
    }
}
