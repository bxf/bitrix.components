<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
use \Bitrix\Main\Localization\Loc;
$arTemplateParameters = array(
    "FORM_TITLE"   => Array(
        "NAME"    => Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_PROPERTY_TITLE'),
    ),
);
