<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
*/
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$frame = $this->createFrame()->begin('');
?><div class="holder-subscribe">
    <div class="holder-subscribe__text"><?=htmlspecialchars_decode($arParams['FORM_TITLE'])?></div>
    <form class="holder-subscribe__form" method="post">
        <input type="hidden" name="action" value="subscribe">
        <div class="holder-subscribe__input">
            <input name="FIELDS[NAME]" value="<?=$arResult['FIELDS']['NAME']?>" type="text" placeholder="<?=Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_EMAIL')?>" class="input-text <?=$arResult['MESSAGE']['TYPE']== 'ERROR'? ' is-error' : ''?>">
        </div>
        <div class="holder-subscribe__submit">
            <button type="submit" class="btn btn--big btn--fullw">
                <span><?=Loc::getMessage('BITFACTORY_FORM_SUBSCRIPTION_SEND')?></span>
            </button>
        </div>
        <div class="holder-subscribe__error"><?
            if (isset($arResult['MESSAGE'])) {
                ShowMessage($arResult['MESSAGE']);
            }
        ?></div>
    </form>
</div><?
