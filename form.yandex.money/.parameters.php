<?
/** BitrixVars
 * @var array $arCurrentValues
 * @var string $componentPath
*/
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
use \Bitrix\Main\Localization\Loc;
$arComponentParameters = array(
    "GROUPS" => Array(
        "MAIN" => Array(
            "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_MAIN'),
        ),
        "PERSONAL_DATA" => Array(
            "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PERSONAL_DATA'),
        ),
        "OTHER" => Array(
            "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_OTHER_SETTINGS'),
        ),
    ),
    "PARAMETERS" => Array(),
);

$arComponentParameters['PARAMETERS']['ACCOUNT'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_ACCOUNT'),
	"TYPE" => "TEXT",
    "DEFAULT" => "",
);

$arComponentParameters['PARAMETERS']['PAYMENT_TYPE_CHOICE'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PAYMENT_TYPE_CHOICE'),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters['PARAMETERS']['MOBILE_PAYMENT_TYPE_CHOICE'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_MOBILE_PAYMENT_TYPE_CHOICE'),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters['PARAMETERS']['PURPOSE_OF_PAYMENT'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT'),
    "TYPE" => "LIST",
    "VALUES" => Array(
        'seller' => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT_SELLER'),
        'buyer' => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT_BUYER'),
    ),
    "DEFAULT" => "seller",
    "REFRESH" => "Y",
);

switch ($arCurrentValues['PURPOSE_OF_PAYMENT']) {
    case 'buyer':
        $arComponentParameters['PARAMETERS']['PURPOSE_OF_PAYMENT_HINT'] = Array(
            "PARENT" => "MAIN",
            "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_HINT'),
            "TYPE" => "TEXT",
        );
        break;
    default:
    //case 'seller':
        $arComponentParameters['PARAMETERS']['PURPOSE_OF_PAYMENT_VALUE'] = Array(
            "PARENT" => "MAIN",
            "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT_VALUE'),
            "TYPE" => "TEXT",
        );
        break;
}

$arComponentParameters['PARAMETERS']['SUM'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_DEFAULT_SUM'),
    "TYPE" => "TEXT",
    "DEFAULT" => "",
);

$arComponentParameters['PARAMETERS']['BUTTON_TEXT'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_BUTTON_TEXT'),
    "TYPE" => "LIST",
    "VALUES" => Array(
        '01' => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PAY'),
        '03' => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_SEND'),
        '02' => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_BUY'),
        '04' => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_GIFT'),
    ),
    "DEFAULT" => "01",
);

$arComponentParameters['PARAMETERS']['COMMENT'] = Array(
    "PARENT" => "MAIN",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_COMMENT'),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
    "REFRESH" => "Y",
);

if ($arCurrentValues['COMMENT'] != 'N') {
    $arComponentParameters['PARAMETERS']['COMMENT_HINT'] = Array(
        "PARENT" => "MAIN",
        "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_HINT'),
        "TYPE" => "TEXT",
    );
}

$arComponentParameters['PARAMETERS']['FIO'] = Array(
    "PARENT" => "PERSONAL_DATA",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_FIO'),
    "TYPE" => "CHECKBOX",
);

$arComponentParameters['PARAMETERS']['PHONE'] = Array(
    "PARENT" => "PERSONAL_DATA",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_PHONE'),
    "TYPE" => "CHECKBOX",
);

$arComponentParameters['PARAMETERS']['EMAIL'] = Array(
    "PARENT" => "PERSONAL_DATA",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_EMAIL'),
    "TYPE" => "CHECKBOX",
);

$arComponentParameters['PARAMETERS']['ADDRESS'] = Array(
    "PARENT" => "PERSONAL_DATA",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_ADDRESS'),
    "TYPE" => "CHECKBOX",
);

$arComponentParameters['PARAMETERS']['REDIRECT_PATH'] = Array(
    "PARENT" => "OTHER",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_REDIRECT_PATH'),
    "DEFAULT" => "http://".$_SERVER['HTTP_HOST'],
);

$arComponentParameters['PARAMETERS']['WIDTH'] = Array(
    "PARENT" => "OTHER",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_WIDTH'),
    "DEFAULT" => "450",
);

$arComponentParameters['PARAMETERS']['HEIGHT'] = Array(
    "PARENT" => "OTHER",
    "NAME" => Loc::getMessage('BITFACTORY_FORM_YANDEX_MONEY_HEIGHT'),
    "DEFAULT" => "279",
);
