<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponent $this
*/
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
$arReqParams = Array(
    'quickpay' => 'shop',
);
if (strlen($arParams['ACCOUNT'])) {
    $arReqParams['account'] = trim($arParams['ACCOUNT']);
}
if ($arParams['PAYMENT_TYPE_CHOICE'] == 'Y') {
    $arReqParams['payment-type-choice'] = 'on';
}

if ($arParams['MOBILE_PAYMENT_TYPE_CHOICE'] == 'Y') {
    $arReqParams['mobile-payment-type-choice'] = 'on';
}

$arReqParams['writer'] = $arParams['PURPOSE_OF_PAYMENT'] == 'buyer' ? 'buyer' : 'seller';

switch($arParams['PURPOSE_OF_PAYMENT']) {
    case 'buyer':
        $arReqParams['targets-hint'] = trim($arParams['PURPOSE_OF_PAYMENT_HINT']);
        break;
    default: // seller
        $arReqParams['targets'] = trim($arParams['PURPOSE_OF_PAYMENT_VALUE']);
}

if (strlen($arParams['SUM'])) {
   $arReqParams['default-sum'] = trim($arParams['SUM']);
}

if (strlen($arParams['BUTTON_TEXT'])) {
    $arReqParams['button-text'] = trim($arParams['BUTTON_TEXT']);
}

if ($arParams['COMMENT'] == 'Y') {
    $arReqParams['comment'] = 'on';
}

if (strlen($arParams['COMMENT_HINT'])) {
    $arReqParams['hint'] = trim($arParams['COMMENT_HINT']);
}

if ($arParams['FIO'] == 'Y') {
    $arReqParams['fio'] = 'on';
}
if ($arParams['EMAIL'] == 'Y') {
    $arReqParams['mail'] = 'on';
}
if ($arParams['PHONE'] == 'Y') {
    $arReqParams['phone'] = 'on';
}
if ($arParams['ADDRESS'] == 'Y') {
    $arReqParams['address'] = 'on';
}

if (strlen($arParams['REDIRECT_PATH'])) {
    $arReqParams['successURL'] = trim($arParams['REDIRECT_PATH']);
}

$arReqURI = Array();
foreach($arReqParams as $strKey => $strValue) {
    $arReqURI[] = $strKey . '=' . urlencode($strValue);
}

$strReqURI = 'https://money.yandex.ru/embed/shop.xml?' . implode('&', $arReqURI);

if (!(int)$arParams['WIDTH']) {
    $arParams['WIDTH'] = 450;
}

if (!(int)$arParams['HEIGHT']) {
    $arParams['HEIGHT'] = 279;
}

?><iframe frameborder="0" allowtransparency="true" scrolling="no" src="<?=$strReqURI?>" width="<?=$arParams['WIDTH']?>" height="<?=$arParams['HEIGHT']?>"></iframe><?
