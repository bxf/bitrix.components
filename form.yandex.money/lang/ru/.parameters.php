<?php
$MESS['BITFACTORY_FORM_YANDEX_MONEY_MAIN'] = 'Основные настройки';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_OTHER_SETTINGS'] = 'Прочие настройки';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_ACCOUNT'] = 'Номер счета';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PAYMENT_TYPE_CHOICE'] = 'Платежи с карт Visa и MasterCard';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_MOBILE_PAYMENT_TYPE_CHOICE'] = 'Платежи со счета мобильного телефона';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT'] = 'Назначение платежа';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT_VALUE'] = 'Что показывать в назначении';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT_SELLER'] = 'указать сразу';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PURPOSE_OF_PAYMENT_BUYER'] = 'укажет плательщик';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_HINT'] = 'Подсказка к полю';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_DEFAULT_SUM'] = 'Сумма по умолчанию';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_BUTTON_TEXT'] = 'Текст на кнопке';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_SEND'] = 'Перевести';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_BUY'] = 'Купить';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PAY'] = 'Оплатить';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_GIFT'] = 'Подарить';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_COMMENT'] = 'Комментарий плательщика';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PERSONAL_DATA'] = 'Данные плательщика';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_FIO'] = 'ФИО';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_EMAIL'] = 'email';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_PHONE'] = 'телефон';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_ADDRESS'] = 'адрес доставки';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_REDIRECT_PATH'] = 'Адрес для редиректа';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_WIDTH'] = 'Ширина';
$MESS['BITFACTORY_FORM_YANDEX_MONEY_HEIGHT'] = 'Высота';
